<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Create Participant</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<div class="container">
    <form method="post" action="/app/participant/create">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Create new Participant</h4>
            </div>
            <div class="panel-body">
                <c:if test="${not empty error}">
                    <span class="error">${error}</span>
                </c:if>
                <div class="form-group">
                    <label for="riderNameInput">Rider Name</label>
                    <input class="form-control" id="riderNameInput" type="text" name="riderName" value="${riderName}"/>
                    <c:if test="${not empty riderName_error}">
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                                ${riderName_error}
                        </div>
                    </c:if>
                </div>
                <div class="form-group">
                    <label for="horseNameInput">Horse Name</label>
                    <input class="form-control" id="horseNameInput" type="text" name="horseName" value="${horseName}"/>
                    <c:if test="${not empty horseName_error}">
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                                ${horseName_error}
                        </div>
                    </c:if>
                </div>
                <input type="submit" value="Save" class="btn btn-primary"/>
            </div>
        </div>
    </form>
</div>
</body>
</html>
