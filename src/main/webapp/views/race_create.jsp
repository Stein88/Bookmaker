<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Create Race</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <form method="post" action="/app/race/create">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Create new Race</h4>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="idInput">ID</label>
                    <input class="form-control" id="idInput" type="text" name="id" readonly/>
                </div>
                <div class="form-group">
                    <label for="raceDate">Race date</label>
                    <input class="form-control" id="raceDate" type="text" name="raceDate" value="${raceDate}"/>
                    <c:if test="${not empty raceDate_error}">
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            ${raceDate_error}
                        </div>
                    </c:if>
                </div>
                <div class="form-group">
                    <label for="exampleSelect2">Choose participant </label>
                    <select multiple class="form-control" id="exampleSelect2" name="participantIds">
                        <c:forEach items="${participants}" var="participant">
                            <option value="${participant.id}">${participant.riderName} - ${participant.horseName}</option>
                        </c:forEach>
                    </select>
                    <c:if test="${not empty participantIds_error}">
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            ${participantIds_error}
                        </div>
                    </c:if>
                </div>
                <input type="submit" value="Save" class="btn btn-primary"/>
            </div>
        </div>
    </form>
</div>
</body>
</html>
