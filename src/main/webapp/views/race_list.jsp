<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Rice List</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<table class="table">
    <thead>
    <tr>
        <th>ID</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${races}" var="race">
        <tr>
            <td><a href="/app/race/detail?id=${race.id}">${race.id}</a></td>
            <td>${race.date}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
