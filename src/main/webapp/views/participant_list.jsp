<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Participants List</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<table class="table">
    <thead>
    <tr>
        <th>Rider Name</th>
        <th>Horse Name</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${participants}" var="participant">
        <tr>
            <td>${participant.riderName}</td>
            <td>${participant.horseName}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>
