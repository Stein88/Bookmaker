<%--
  Created by IntelliJ IDEA.
  User: oleksii
  Date: 28.11.16
  Time: 14:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Registration page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<div class="container">
    <form method="post" action="/app/registration">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Register page</h4>
            </div>
            <div class="panel-body">
                <c:if test="${error != null}">
                    <div class="form-group">
                        <label class="error">${error}</label>
                    </div>
                </c:if>
                <div class="form-group">
                    <label for="nameInput">name</label>
                    <input class="form-control" id="nameInput" type="text" name="name" value="${name}"/>
                    <c:if test="${not empty name_error}">
                        <span class="error">${name_error}</span>
                    </c:if>
                </div>
                <div class="form-group">
                    <label for="emailInput">email</label>
                    <input class="form-control" id="emailInput" type="text" name="email" value="${email}"/>
                    <c:if test="${not empty email_error}">
                        <span class="error">${email_error}</span>
                    </c:if>
                </div>
                <div class="form-group">
                    <label for="passwordInput">Password</label>
                    <input class="form-control" id="passwordInput" type="password" name="password" value="${password}"/>
                    <c:if test="${not empty password_error}">
                        <span class="error">${password_error}</span>
                    </c:if>
                </div>
                <input type="submit" value="Register" class="btn btn-primary"/>
            </div>
        </div>
    </form>
</div>

</body>
</html>
