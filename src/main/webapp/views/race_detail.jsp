<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://bookmaker.lawless.com/tags" prefix="cm" %>
<html>
<head>
    <title>Race Detail</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Race detail</h4>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="idInput">ID</label>
                <input value="${race.id}" class="form-control" id="idInput" type="text" name="id" readonly/>
            </div>
            <div class="form-group">
                <label for="raceDate">Race date</label>
                <fmt:formatDate value="${race.date}" var="fmtDate" pattern="dd/MM/yyyy HH:mm"/>
                <input value="${fmtDate}" class="form-control" id="raceDate" type="text" name="raceDate" readonly/>
            </div>
            <div class="form-group">
                <label>Participants</label>
                <ul class="list-group">
                    <c:forEach items="${race.raceParticipants}" var="raceParticipant">
                        <li class="list-group-item">
                                ${raceParticipant.participant.riderName} - ${raceParticipant.participant.horseName};
                            coefficient ${raceParticipant.coefficient}
                            <c:if test="${!completed}">
                                <c:if test="${user.role == 'BOOKMAKER' || user.role == 'ADMIN'}">
                                    <form method="post" style="display: inline-block;"
                                          action="/app/race/set_coefficient?racePartId=${raceParticipant.id}">
                                        <input title="coefficient" type="text" name="coefficient"
                                               <c:if test="${failedCoefficientRacePartId eq raceParticipant.id}">value="${failedCoefficient}"
                                               style="color: red; border-color: red" </c:if>/>
                                        <button type="submit" class="btn btn-default btn-sm">Set</button>
                                    </form>
                                </c:if>
                                <form method="post" style="display: inline-block;"
                                      action="/app/race/set_bet?racePartId=${raceParticipant.id}">
                                    <input title="bet" type="text" name="bet"
                                           <c:if test="${failedBetRacePartId eq raceParticipant.id}">value="${failedBet}"
                                           style="color: red; border-color: red" </c:if>/>
                                    <button type="submit" class="btn btn-default btn-sm">Bet</button>
                                </form>
                                <c:if test="${user.role == 'ADMIN'}">
                                    <a href="/app/race/set_win?racePartId=${raceParticipant.id}"
                                       class="btn btn-success btn-sm">Winner</a>
                                </c:if>
                            </c:if>
                            <br/>Your bets:
                            <c:forEach items="${raceParticipant.bets}" var="bet">
                                ${bet.amount};
                            </c:forEach>
                            <c:if test="${raceParticipant.win}">
                                <br/>
                                <span style="background-color: green">Winner</span>
                                <br/>
                                Your win: <cm:win bets="${raceParticipant.bets}"
                                                  coefficient="${raceParticipant.coefficient}"/>
                            </c:if>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
