package com.lawless.bookmaker.web;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.lawless.bookmaker.web.HttpMethod.*;

/**
 * Created by Stein on 24.11.16.
 */
public class DispatcherServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(DispatcherServlet.class);
    private static DispatcherContext context = DispatcherContext.getInstance();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doMagic(GET, request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doMagic(POST, request, response);
    }

    private void doMagic(HttpMethod method, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("Send " + method.getName() + " to " + request.getPathInfo());
        DispatcherModel model = new DispatcherModel();
        model.setParameters(request.getParameterMap());
        model.setSession(request.getSession());
        String path = executeBusinessLogic(method, model, request, response);

        for (Map.Entry<String, Object> attributes : model.getAttributes().entrySet()) {
            request.setAttribute(attributes.getKey(), attributes.getValue());
        }

        if (!redirectIfNecessary(path, request, response)) {
            request.getRequestDispatcher(path).forward(request, response);
        }
    }

    private boolean redirectIfNecessary(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (url.startsWith("redirect:")) {
            response.sendRedirect(url.substring(url.indexOf(":") + 1));
            return true;
        }
        return false;
    }

    private String executeBusinessLogic(HttpMethod method, Model model, HttpServletRequest request, HttpServletResponse response) {
        Command command = context.findCommand(request.getPathInfo(), method);
        if (command != null) {
            try {
                return command.execute(model);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return "/views/error.jsp";
            }
        } else {
            return "/views/404.jsp";
        }
    }

}
