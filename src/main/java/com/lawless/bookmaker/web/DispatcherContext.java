package com.lawless.bookmaker.web;

import com.lawless.bookmaker.commands.*;
import com.lawless.bookmaker.commands.participant.CreateParticipantCommand;
import com.lawless.bookmaker.commands.participant.ShowCreateParticipantPageCommand;
import com.lawless.bookmaker.commands.participant.ShowParticipantListCommand;
import com.lawless.bookmaker.commands.race.CreateRaceCommand;
import com.lawless.bookmaker.commands.race.ShowCreateRacePageCommand;
import com.lawless.bookmaker.commands.race.ShowRaceDetailCommand;
import com.lawless.bookmaker.commands.race.ShowRaceListCommand;
import com.lawless.bookmaker.commands.bet.SetBetCommand;
import com.lawless.bookmaker.commands.race_participant.SetCoefficientCommand;
import com.lawless.bookmaker.commands.race_participant.SetWinnerCommand;
import com.lawless.bookmaker.commands.user.LoginUserCommand;
import com.lawless.bookmaker.commands.user.RegisterNewUserCommand;
import com.lawless.bookmaker.commands.user.ShowUserListCommand;
import com.lawless.bookmaker.common.Command;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 24.11.16.
 */
public class DispatcherContext {
    private static DispatcherContext instance = new DispatcherContext();

    private Map<String, Map<HttpMethod, Command>> commands;

    private DispatcherContext() {
        initControllers();
    }

    static DispatcherContext getInstance() {
        return instance;
    }

    private void initControllers() {
        commands = new ContextBuilder()
                .register("/race/list", new ShowRaceListCommand())
                .register("/race/detail", new ShowRaceDetailCommand())
                .register("/race/set_coefficient", HttpMethod.POST, new SetCoefficientCommand())
                .register("/race/set_bet", HttpMethod.POST, new SetBetCommand())
                .register("/race/set_win", new SetWinnerCommand())
                .register("/race/create", HttpMethod.POST, new CreateRaceCommand())
                .register("/race/create", new ShowCreateRacePageCommand())
                .register("/login", new ShowLoginPageCommand())
                .register("/login", HttpMethod.POST, new LoginUserCommand())
                .register("/registration", new ShowRegisterPageCommand())
                .register("/registration", HttpMethod.POST, new RegisterNewUserCommand())
                .register("/user/list", new ShowUserListCommand())
                .register("/participant/list", new ShowParticipantListCommand())
                .register("/participant/create", new ShowCreateParticipantPageCommand())
                .register("/participant/create", HttpMethod.POST, new CreateParticipantCommand())
                .build();
    }

    Command findCommand(String path, HttpMethod method) {
        Map<HttpMethod, Command> commandsMap = commands.get(path);
        Command command = null;
        if (commandsMap != null && commandsMap.containsKey(method)) {
            command = commandsMap.get(method);
        }
        return command;
    }

    private class ContextBuilder {
        private Map<String, Map<HttpMethod, Command>> commands = new HashMap<>();

        private ContextBuilder register(String path, Command command) {
            return register(path, HttpMethod.GET, command);
        }

        private ContextBuilder register(String path, HttpMethod method, Command command) {
            Map<HttpMethod, Command> map = findCommandMap(path);
            map.put(method, command);
            return this;
        }

        private Map<HttpMethod, Command> findCommandMap(String path) {
            Map<HttpMethod, Command> map;
            if (commands.containsKey(path)) {
                map = commands.get(path);
            } else {
                map = new HashMap<>();
            }
            commands.put(path, map);
            return map;
        }

        private Map<String, Map<HttpMethod, Command>> build() {
            return commands;
        }
    }
}
