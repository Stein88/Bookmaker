package com.lawless.bookmaker.web.custom_tags;

import com.lawless.bookmaker.domain.Bet;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.List;

public class SummTag extends SimpleTagSupport {
    private List<Bet> bets;
    private Integer coefficient;

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        Long result = 0L;
        for (Bet bet : bets) {
            result += bet.getAmount();
        }
        result *= coefficient;
        out.println(result);
    }


    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public Integer getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Integer coefficient) {
        this.coefficient = coefficient;
    }
}
