package com.lawless.bookmaker.web;

import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.common.parsers.Parser;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by Stein on 24.11.16.
 */
public class DispatcherModel implements Model {
    private Map<String, Object> attributes = new HashMap<>();
    private Map<String, String[]> parameters;
    private HttpSession session;

    @Override
    public void setAttribute(String key, Object o) {
        attributes.put(key, o);
    }

    @Override
    public void setAttributes(Map<String, Object> attributes) {
        this.attributes.putAll(attributes);
    }

    @Override
    public String findParameter(String key) {
        String[] params = parameters.get(key);
        if (params == null || params.length == 0) {
            throw new IllegalArgumentException("Enter parameter");
        }
        return params[0];
    }

    @Override
    public <T> T findParameter(String key, Parser<T> parser) {
        return parser.parse(key, parameters.get(key));
    }

    @Override
    public void putSessionAttribute(String s, Object o) {
        session.setAttribute(s, o);
    }

    @Override
    public Object getSessionAttribute(String s) {
        return session.getAttribute(s);
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setParameters(Map<String, String[]> parameters) {
        this.parameters = parameters;
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

}
