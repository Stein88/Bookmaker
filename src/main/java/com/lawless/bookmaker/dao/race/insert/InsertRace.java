package com.lawless.bookmaker.dao.race.insert;

import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.jdbc.InsertQueryCommand;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Created by Stein on 04.12.16.
 */
public class InsertRace implements InsertQueryCommand<Race> {
    private static final String QUERY = "INSERT INTO race(date) VALUES (?)";
    private Race race;

    public InsertRace(Race race) {
        this.race = race;
    }


    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setTimestamp(1, new Timestamp(race.getDate().getTime()));
    }

    @Override
    public Race extractInserted(Long generatedId) throws SQLException {
        race.setId(generatedId);
        return race;
    }
}
