package com.lawless.bookmaker.dao.race;

import com.lawless.bookmaker.dao.bet.select.SelectBetsByRaceParticipant;
import com.lawless.bookmaker.dao.race_participant.insert.InsertRaceParticipant;
import com.lawless.bookmaker.dao.race.select.SelectAllRaces;
import com.lawless.bookmaker.dao.race.insert.InsertRace;
import com.lawless.bookmaker.dao.race.select.SelectRaceById;
import com.lawless.bookmaker.dao.race_participant.select.SelectRaceParticipantsByRace;
import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.domain.User;
import com.lawless.bookmaker.jdbc.DataBaseUtility;

import java.util.*;

/**
 * Created by Stein on 27.11.16.
 */
public class RaceDaoImpl implements RaceDao {
    private static RaceDao instance = new RaceDaoImpl();

    private RaceDaoImpl() {
    }

    public static RaceDao getInstance() {
        return instance;
    }

    public Race create(Race race) {
        Race savedRace = DataBaseUtility.execute(new InsertRace(race));
        List<RaceParticipant> savedRaceParticipants = new ArrayList<>();
        if (savedRace == null || savedRace.getRaceParticipants() == null || savedRace.getRaceParticipants().isEmpty()) {
            return savedRace;
        }
        for (RaceParticipant raceParticipant : savedRace.getRaceParticipants()) {
            RaceParticipant savedRaceParticipant = DataBaseUtility.execute(new InsertRaceParticipant(savedRace, raceParticipant));
            savedRaceParticipants.add(savedRaceParticipant);
        }
        savedRace.setRaceParticipants(savedRaceParticipants);
        return savedRace;
    }

    public Race read(Long id, User user) {
        Race race = DataBaseUtility.execute(new SelectRaceById(id));
        race.setRaceParticipants(DataBaseUtility.execute(new SelectRaceParticipantsByRace(race)));
        for (RaceParticipant raceParticipant : race.getRaceParticipants()) {
            raceParticipant.setBets(DataBaseUtility.execute(new SelectBetsByRaceParticipant(raceParticipant, user)));
        }
        return race;
    }

    public Collection<Race> readAll() {
        return DataBaseUtility.execute(new SelectAllRaces());
    }

    public Race update(Long id, Race e) {
        return null;
    }

    public Race delete(Long id) {
        return null;
    }

}
