package com.lawless.bookmaker.dao.race;


import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.User;

import java.util.Collection;

/**
 * Created by Stein on 24.11.16.
 */
public interface RaceDao {
    Race create(Race race);

    Race read(Long id, User user);

    Race update(Long id, Race race);

    Race delete(Long id);

    Collection<Race> readAll();
}
