package com.lawless.bookmaker.dao.race.select;

import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Stein on 04.12.16.
 */
public class SelectAllRaces implements SelectQueryCommand<Collection<Race>> {
    private static final String QUERY = "SELECT * FROM race";

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        // Do nothing
    }

    @Override
    public Collection<Race> parseResultSet(ResultSet resultSet) throws SQLException {
        Collection<Race> racesList = new ArrayList<>();
        while (resultSet.next()) {
            Race race = new Race();
            race.setId(resultSet.getLong("race_id"));
            race.setDate(resultSet.getDate("date"));
            racesList.add(race);
        }
        return racesList;
    }
}
