package com.lawless.bookmaker.dao.race.select;

import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Stein on 04.12.16.
 */
public class SelectRaceById implements SelectQueryCommand<Race> {
    private static final String QUERY = "SELECT * FROM race WHERE race_id = ?";
    private Long id;

    public SelectRaceById(Long id) {
        this.id = id;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, id);
    }

    @Override
    public Race parseResultSet(ResultSet resultSet) throws SQLException {
        Race race = new Race();
        while (resultSet.next()) {
            race.setId(resultSet.getLong("race_id"));
            race.setDate(new Date(resultSet.getTimestamp("date").getTime()));
        }
        return race;
    }
}
