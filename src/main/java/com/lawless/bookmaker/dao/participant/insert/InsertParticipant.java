package com.lawless.bookmaker.dao.participant.insert;

import com.lawless.bookmaker.domain.Participant;
import com.lawless.bookmaker.jdbc.InsertQueryCommand;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Stein on 04.12.16.
 */
public class InsertParticipant implements InsertQueryCommand<Participant> {
    private static final String QUERY = "INSERT INTO participant(rider_name, horse_name) VALUES (?, ?)";
    private Participant participant;

    public InsertParticipant(Participant participant) {
        this.participant = participant;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, participant.getRiderName());
        preparedStatement.setString(2, participant.getHorseName());
    }

    @Override
    public Participant extractInserted(Long generatedId) throws SQLException {
        participant.setId(generatedId);
        return participant;
    }
}
