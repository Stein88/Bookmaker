package com.lawless.bookmaker.dao.participant.select;

import com.lawless.bookmaker.domain.Participant;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Stein on 05.12.16.
 */
public class SelectParticipantById implements SelectQueryCommand<Participant> {
    private static final String QUERY = "SELECT * FROM participant WHERE participant_id = ?";
    private Long id;

    public SelectParticipantById(Long id) {
        this.id = id;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }


    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, id);
    }

    @Override
    public Participant parseResultSet(ResultSet resultSet) throws SQLException {
        Participant participant = new Participant();
        while (resultSet.next()) {
            participant.setId(resultSet.getLong("participant_id"));
            participant.setRiderName(resultSet.getString("rider_name"));
            participant.setHorseName(resultSet.getString("horse_name"));
        }
        return participant;
    }
}
