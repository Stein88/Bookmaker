package com.lawless.bookmaker.dao.participant;

import com.lawless.bookmaker.domain.Participant;

import java.util.Collection;
import java.util.List;

/**
 * Created by Stein on 24.11.16.
 */
public interface ParticipantDao {
    Participant create(Participant participant);

    Participant update(Long id, Participant participant);

    Participant read(Long id);

    Participant delete(Long id);

    Collection<Participant> readAll();

    Collection<Participant> findByIds(List<Long> ids);

    Participant findByNameAndHorse(String riderName, String horseName);
}

