package com.lawless.bookmaker.dao.participant.select;

import com.lawless.bookmaker.domain.Participant;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Stein on 05.12.16.
 */
public class SelectParticipantByNameAndHorse implements SelectQueryCommand<Participant> {
    private static final String QUERY = "SELECT * FROM participant WHERE rider_name = ? AND horse_name = ?";
    private String riderName;
    private String horseName;

    public SelectParticipantByNameAndHorse(String riderName, String horseName) {
        this.riderName = riderName;
        this.horseName = horseName;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, riderName);
        preparedStatement.setString(2, horseName);
    }

    @Override
    public Participant parseResultSet(ResultSet resultSet) throws SQLException {
        Participant participant = null;
        while (resultSet.next()) {
            participant = new Participant();
            participant.setId(resultSet.getLong("participant_id"));
            participant.setRiderName(resultSet.getString("rider_name"));
            participant.setHorseName(resultSet.getString("horse_name"));
        }
        return participant;
    }
}
