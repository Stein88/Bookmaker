package com.lawless.bookmaker.dao.participant.select;

import com.lawless.bookmaker.domain.Participant;
import com.lawless.bookmaker.jdbc.AbstractSelectQuery;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by Stein on 05.12.16.
 */
public class SelectAllParticipant extends AbstractSelectQuery implements SelectQueryCommand<Collection<Participant>> {
    private static final String QUERY = "SELECT * FROM participant";

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        //do nothing
    }

    @Override
    public Collection<Participant> parseResultSet(ResultSet resultSet) throws SQLException {
        return obtainParticipants(resultSet);
    }
}
