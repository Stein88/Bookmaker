package com.lawless.bookmaker.dao.participant;

import com.lawless.bookmaker.dao.participant.insert.InsertParticipant;
import com.lawless.bookmaker.dao.participant.select.SelectAllParticipant;
import com.lawless.bookmaker.dao.participant.select.SelectParticipantById;
import com.lawless.bookmaker.dao.participant.select.SelectParticipantByNameAndHorse;
import com.lawless.bookmaker.domain.Participant;
import com.lawless.bookmaker.jdbc.DataBaseUtility;

import java.util.*;

/**
 * Created by Stein on 27.11.16.
 */
public class ParticipantDaoImpl implements ParticipantDao {
    private static ParticipantDao instance = new ParticipantDaoImpl();

    private ParticipantDaoImpl() {
    }

    public static ParticipantDao getInstance() {
        return instance;
    }

    @Override
    public Participant create(Participant participant) {
        return DataBaseUtility.execute(new InsertParticipant(participant));
    }

    @Override
    public Participant update(Long id, Participant participant) {
        return null;
    }

    @Override
    public Participant read(Long id) {
        return DataBaseUtility.execute(new SelectParticipantById(id));
    }


    @Override
    public Participant delete(Long id) {
        return null;
    }

    @Override
    public Collection<Participant> readAll() {
        return DataBaseUtility.execute(new SelectAllParticipant());
    }

    @Override
    public Participant findByNameAndHorse(String riderName, String horseName) {
        return DataBaseUtility.execute(new SelectParticipantByNameAndHorse(riderName, horseName));
    }

    @Override
    public Collection<Participant> findByIds(List<Long> ids) {
        List<Participant> listParticipants = new ArrayList<>();
        for (Long id : ids) {
            listParticipants.add(read(id));
        }
        return listParticipants;
    }
}
