package com.lawless.bookmaker.dao.user.select;

import com.lawless.bookmaker.domain.User;
import com.lawless.bookmaker.domain.UserRole;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Stein on 05.12.16.
 */
public class SelectUserByLogin implements SelectQueryCommand<User> {
    private static final String QUERY = "SELECT * FROM user WHERE email = ?";
    private String login;

    public SelectUserByLogin(String login) {
        this.login = login;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, login);
    }

    @Override
    public User parseResultSet(ResultSet resultSet) throws SQLException {
        User user = null;
        while (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getLong("user_id"));
            user.setName(resultSet.getString("first_name"));
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(UserRole.fromString(resultSet.getString("role")));
        }
        return user;
    }
}
