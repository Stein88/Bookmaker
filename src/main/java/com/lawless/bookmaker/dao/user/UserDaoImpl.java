package com.lawless.bookmaker.dao.user;

import com.lawless.bookmaker.dao.user.insert.InsertUser;
import com.lawless.bookmaker.dao.user.select.SelectAllUsers;
import com.lawless.bookmaker.dao.user.select.SelectUserByLogin;
import com.lawless.bookmaker.domain.User;
import com.lawless.bookmaker.jdbc.DataBaseUtility;

import java.util.*;

/**
 * Created by Stein on 27.11.16.
 */
public class UserDaoImpl implements UserDao {
    private static UserDao instance = new UserDaoImpl();

    public static UserDao getInstance() {
        return instance;
    }

    public User create(User user) {
        return DataBaseUtility.execute(new InsertUser(user));
    }

    public User read(Long id) {
        return null;
    }

    public User update(Long id, User e) {
        return null;
    }

    public User delete(Long id) {
        return null;
    }

    public Collection<User> readAll() {
        return DataBaseUtility.execute(new SelectAllUsers());
    }

    public User findByLogin(String login) {
        return DataBaseUtility.execute(new SelectUserByLogin(login));
    }
}
