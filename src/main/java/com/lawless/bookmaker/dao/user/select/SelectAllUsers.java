package com.lawless.bookmaker.dao.user.select;

import com.lawless.bookmaker.domain.User;
import com.lawless.bookmaker.domain.UserRole;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Stein on 05.12.16.
 */
public class SelectAllUsers implements SelectQueryCommand<Collection<User>> {
    private final static String QUERY = "SELECT * FROM user";

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        //do nothing
    }

    @Override
    public Collection<User> parseResultSet(ResultSet resultSet) throws SQLException {
        Collection<User> usersList = new ArrayList<>();
        while (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getLong("user_id"));
            user.setName(resultSet.getString("first_name"));
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(UserRole.fromString(resultSet.getString("role")));
            usersList.add(user);
        }
        return usersList;
    }
}
