package com.lawless.bookmaker.dao.user;

import com.lawless.bookmaker.dao.user.insert.InsertUser;
import com.lawless.bookmaker.dao.user.select.SelectAllUsers;
import com.lawless.bookmaker.domain.User;
import com.lawless.bookmaker.jdbc.DataBaseUtility;

import java.util.Collection;

/**
 * Created by Stein on 24.11.16.
 */
public interface UserDao {

    User create(User user);

    User read(Long id);

    User update(Long id, User e);

    User delete(Long id);

    Collection<User> readAll();

    User findByLogin(String login);
}
