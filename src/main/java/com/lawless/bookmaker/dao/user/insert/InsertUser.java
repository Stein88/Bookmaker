package com.lawless.bookmaker.dao.user.insert;

import com.lawless.bookmaker.domain.User;
import com.lawless.bookmaker.jdbc.InsertQueryCommand;


import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Stein on 05.12.16.
 */
public class InsertUser implements InsertQueryCommand<User> {
    private static final String QUERY = "INSERT INTO user(first_name, email, password, role) VALUES (?, ?, ?, ?)";
    private User user;

    public InsertUser(User user) {
        this.user = user;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, user.getName());
        preparedStatement.setString(2, user.getEmail());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.setString(4, user.getRole().toString());
    }

    @Override
    public User extractInserted(Long generatedId) throws SQLException {
        user.setId(generatedId);
        return user;
    }

}
