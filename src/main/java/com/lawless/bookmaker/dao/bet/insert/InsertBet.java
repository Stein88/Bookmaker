package com.lawless.bookmaker.dao.bet.insert;

import com.lawless.bookmaker.domain.Bet;
import com.lawless.bookmaker.jdbc.InsertQueryCommand;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Stein on 16.12.16.
 */
public class InsertBet implements InsertQueryCommand<Bet> {
    private static final String QUERY = "INSERT INTO bet(amount, race_participant_id, user_id ) VALUES (?, ?, ?)";
    private Bet bet;

    public InsertBet(Bet bet) {
        this.bet = bet;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, bet.getAmount());
        preparedStatement.setLong(2, bet.getRaceParticipant().getId());
        preparedStatement.setLong(3, bet.getUser().getId());
    }

    @Override
    public Bet extractInserted(Long id) throws SQLException {
        bet.setId(id);
        return bet;
    }
}
