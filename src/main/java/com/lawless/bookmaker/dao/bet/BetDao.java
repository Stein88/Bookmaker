package com.lawless.bookmaker.dao.bet;


import com.lawless.bookmaker.domain.Bet;
import com.lawless.bookmaker.domain.Participant;

import java.util.Collection;
import java.util.List;

/**
 * Created by Stein on 24.11.16.
 */
public interface BetDao {
    Bet create(Bet bet);

    Bet read(Long id);

    Bet update(Long id, Bet bet);

    Bet delete(Long id);

    Collection<Bet> readAll();
}
