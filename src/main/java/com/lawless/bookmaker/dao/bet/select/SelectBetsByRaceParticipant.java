package com.lawless.bookmaker.dao.bet.select;

import com.lawless.bookmaker.domain.Bet;
import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.domain.User;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Stein on 16.12.16.
 */
public class SelectBetsByRaceParticipant implements SelectQueryCommand<Collection<Bet>> {
    private static final String QUERY = "SELECT * FROM bet WHERE race_participant_id = ? AND user_id = ?";
    private RaceParticipant raceParticipant;
    private User user;

    public SelectBetsByRaceParticipant(RaceParticipant raceParticipant, User user) {
        this.raceParticipant = raceParticipant;
        this.user = user;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, raceParticipant.getId());
        preparedStatement.setLong(2, user.getId());
    }

    @Override
    public Collection<Bet> parseResultSet(ResultSet resultSet) throws SQLException {
        Collection<Bet> betList = new ArrayList<>();
        while (resultSet.next()) {
            Bet bet = new Bet();
            bet.setAmount(resultSet.getLong("amount"));
            betList.add(bet);
        }
        return betList;
    }
}
