package com.lawless.bookmaker.dao.bet;

import com.lawless.bookmaker.dao.bet.insert.InsertBet;
import com.lawless.bookmaker.domain.Bet;
import com.lawless.bookmaker.jdbc.DataBaseUtility;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by Stein on 27.11.16.
 */
public class BetDaoImpl implements BetDao {

    private static BetDao instance = new BetDaoImpl();

    private BetDaoImpl() {
    }

    public static BetDao getInstance() {
        return instance;
    }

    public Bet create(Bet bet) {
        return DataBaseUtility.execute(new InsertBet(bet));
    }

    public Bet read(Long id) {
        return null;
    }

    public Bet update(Long id, Bet bet) {
        return null;
    }

    public Bet delete(Long id) {
        return null;
    }

    public Collection<Bet> readAll() {
        return null;
    }
}
