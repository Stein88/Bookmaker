package com.lawless.bookmaker.dao.race_participant.update;

import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.jdbc.InsertQueryCommand;
import com.lawless.bookmaker.jdbc.UpdateQueryCommand;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Stein on 06.12.16.
 */
public class UpdateRaceParticipant implements UpdateQueryCommand<RaceParticipant> {
    private static final String QUERY = "UPDATE race_participant SET coefficient = ?, win = ? WHERE race_participant_id = ?";
    private RaceParticipant raceParticipant;

    public UpdateRaceParticipant(RaceParticipant raceParticipant) {
        this.raceParticipant = raceParticipant;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, raceParticipant.getCoefficient());
        preparedStatement.setBoolean(2, raceParticipant.isWin());
        preparedStatement.setLong(3, raceParticipant.getId());
    }

    @Override
    public RaceParticipant returnUpdatedObject() throws SQLException {
        return raceParticipant;
    }
}
