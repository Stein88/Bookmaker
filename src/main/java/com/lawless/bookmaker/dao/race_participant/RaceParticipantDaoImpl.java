package com.lawless.bookmaker.dao.race_participant;

import com.lawless.bookmaker.dao.race_participant.select.SelectRaceParticipantsById;
import com.lawless.bookmaker.dao.race_participant.update.UpdateRaceParticipant;
import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.jdbc.DataBaseUtility;

import java.util.*;

/**
 * Created by Stein on 29.11.16.
 */
public class RaceParticipantDaoImpl implements RaceParticipantDao {
    private static RaceParticipantDao instance = new RaceParticipantDaoImpl();

    public static RaceParticipantDao getInstance() {
        return instance;
    }

    public RaceParticipant create(RaceParticipant raceParticipant) {
        return null;
    }

    public RaceParticipant read(Long id) {
        return DataBaseUtility.execute(new SelectRaceParticipantsById(id));
    }

    public RaceParticipant update(RaceParticipant raceParticipant) {
        return DataBaseUtility.execute(new UpdateRaceParticipant(raceParticipant));
    }

    public RaceParticipant delete(Long id) {
        return null;
    }

    public List<RaceParticipant> readAll() {
        return null;
    }

}
