package com.lawless.bookmaker.dao.race_participant.select;

import com.lawless.bookmaker.domain.Participant;
import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Stein on 04.12.16.
 */
public class SelectRaceParticipantsByRace implements SelectQueryCommand<Collection<RaceParticipant>> {
    private static final String QUERY = "SELECT * FROM " +
            "race_participant rp JOIN participant p ON rp.participant_id = p.participant_id " +
            "WHERE rp.race_id = ?";
    private Race race;

    public SelectRaceParticipantsByRace(Race race) {
        this.race = race;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, race.getId());
    }

    @Override
    public Collection<RaceParticipant> parseResultSet(ResultSet resultSet) throws SQLException {
        List<RaceParticipant> raceParticipants = new ArrayList<>();
        while (resultSet.next()) {
            RaceParticipant raceParticipant = new RaceParticipant();
            raceParticipant.setId(resultSet.getLong("race_participant_id"));
            raceParticipant.setCoefficient(resultSet.getInt("coefficient"));
            raceParticipant.setWin(resultSet.getBoolean("win"));
            Participant participant = new Participant();
            participant.setId(resultSet.getLong("participant_id"));
            participant.setRiderName(resultSet.getString("rider_name"));
            participant.setHorseName(resultSet.getString("horse_name"));
            raceParticipant.setParticipant(participant);
            raceParticipant.setRace(race);

            raceParticipants.add(raceParticipant);
        }
        return raceParticipants;
    }
}
