package com.lawless.bookmaker.dao.race_participant.insert;

import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.jdbc.InsertQueryCommand;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Stein on 04.12.16.
 */
public class InsertRaceParticipant implements InsertQueryCommand<RaceParticipant> {
    private static final String QUERY = "INSERT INTO race_participant(race_id, participant_id) VALUES (?, ?)";
    private Race race;
    private RaceParticipant raceParticipant;

    public InsertRaceParticipant(Race race, RaceParticipant raceParticipant) {
        this.race = race;
        this.raceParticipant = raceParticipant;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, race.getId());
        preparedStatement.setLong(2, raceParticipant.getParticipant().getId());
    }

    @Override
    public RaceParticipant extractInserted(Long generatedId) throws SQLException {
        raceParticipant.setId(generatedId);
        return raceParticipant;
    }
}
