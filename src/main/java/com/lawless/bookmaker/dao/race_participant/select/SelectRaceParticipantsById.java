package com.lawless.bookmaker.dao.race_participant.select;

import com.lawless.bookmaker.dao.participant.select.SelectParticipantById;
import com.lawless.bookmaker.dao.race.select.SelectRaceById;
import com.lawless.bookmaker.domain.Participant;
import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.jdbc.DataBaseUtility;
import com.lawless.bookmaker.jdbc.SelectQueryCommand;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Stein on 06.12.16.
 */
public class SelectRaceParticipantsById implements SelectQueryCommand<RaceParticipant> {
    private static final String QUERY = "SELECT * FROM " +
            "race_participant rp JOIN participant p ON rp.participant_id = p.participant_id " +
            "JOIN race r ON r.race_id = rp.race_id " +
            "WHERE race_participant_id = ?";
    private Long id;

    public SelectRaceParticipantsById(Long id) {
        this.id = id;
    }

    @Override
    public String getQuery() {
        return QUERY;
    }

    @Override
    public void setupStatement(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, id);

    }

    @Override
    public RaceParticipant parseResultSet(ResultSet resultSet) throws SQLException {
        RaceParticipant raceParticipant = new RaceParticipant();
        while (resultSet.next()) {
            raceParticipant.setId(resultSet.getLong("race_participant_id"));
            raceParticipant.setCoefficient(resultSet.getInt("coefficient"));
            Race race = new Race();
            race.setId(resultSet.getLong("race_id"));
            race.setDate(resultSet.getDate("date"));
            Participant participant = new Participant();
            participant.setId(resultSet.getLong("participant_id"));
            participant.setRiderName(resultSet.getString("rider_name"));
            participant.setHorseName(resultSet.getString("horse_name"));
            raceParticipant.setRace(race);
            raceParticipant.setParticipant(participant);
        }
        return raceParticipant;
    }
}
