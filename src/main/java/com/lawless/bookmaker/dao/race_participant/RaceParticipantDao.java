package com.lawless.bookmaker.dao.race_participant;

import com.lawless.bookmaker.domain.RaceParticipant;

import java.util.Collection;


/**
 * Created by Stein on 29.11.16.
 */
public interface RaceParticipantDao {
    RaceParticipant create(RaceParticipant raceParticipant);

    RaceParticipant read(Long id);

    RaceParticipant update(RaceParticipant raceParticipant);

    RaceParticipant delete(Long id);

    Collection<RaceParticipant> readAll();

}
