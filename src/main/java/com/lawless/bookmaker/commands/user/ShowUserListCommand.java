package com.lawless.bookmaker.commands.user;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.dao.user.UserDao;
import com.lawless.bookmaker.dao.user.UserDaoImpl;

/**
 * Created by Stein on 27.11.16.
 */
public class ShowUserListCommand implements Command{
    private UserDao userDao = UserDaoImpl.getInstance();

    public String execute(Model model){
        model.setAttribute("users", userDao.readAll());
        return "/views/user_list.jsp";
    }
}
