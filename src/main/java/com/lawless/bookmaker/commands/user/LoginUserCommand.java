package com.lawless.bookmaker.commands.user;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.common.parsers.EmailParser;
import com.lawless.bookmaker.common.parsers.PasswordParser;
import com.lawless.bookmaker.dao.user.UserDao;
import com.lawless.bookmaker.dao.user.UserDaoImpl;
import com.lawless.bookmaker.domain.User;
import com.lawless.bookmaker.domain.UserRole;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 28.11.16.
 */
public class LoginUserCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(LoginUserCommand.class);
    private UserDao userDao = UserDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        Map<String, Object> errors = new HashMap<>();
        String login = model.findParameter("email", new EmailParser(errors));
        String password = model.findParameter("password", new PasswordParser(errors));
        User user = userDao.findByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            LOGGER.info("Success login for " + user);
            model.putSessionAttribute("user", user);
            return "redirect:/app/race/list";
        }
        LOGGER.info("Unsuccess login for " + (user == null ? "unknown" : user));
        errors.put("error", "Incorrect email or password");
        model.setAttributes(errors);
        return "/views/login.jsp";
    }
}
