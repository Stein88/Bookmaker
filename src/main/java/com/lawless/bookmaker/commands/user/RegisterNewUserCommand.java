package com.lawless.bookmaker.commands.user;

import com.lawless.bookmaker.common.parsers.EmailParser;
import com.lawless.bookmaker.common.parsers.NameParser;
import com.lawless.bookmaker.common.parsers.PasswordParser;
import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.dao.user.UserDao;
import com.lawless.bookmaker.dao.user.UserDaoImpl;
import com.lawless.bookmaker.domain.User;
import com.lawless.bookmaker.domain.UserRole;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 28.11.16.
 */
public class RegisterNewUserCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(RegisterNewUserCommand.class);
    private UserDao userDao = UserDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        Map<String, Object> errors = new HashMap<>();
        String name = model.findParameter("name", new NameParser(errors));
        String email = model.findParameter("email", new EmailParser(errors));
        String password = model.findParameter("password", new PasswordParser(errors));
        User user = userDao.findByLogin(email);
        if (user != null) {
            LOGGER.info("This " + user + " already created");
            errors.put("error", "This user already created");
        }
        if (!errors.isEmpty()) {
            model.setAttribute("name", name);
            model.setAttribute("email", email);
            model.setAttribute("password", password);
            model.setAttributes(errors);
            return "/views/registration.jsp";
        }
        User newUser = new User(name, email, password, UserRole.CLIENT);
        userDao.create(newUser);
        LOGGER.info("Create new " + newUser);
        return "redirect:/app/race/list";
    }
}
