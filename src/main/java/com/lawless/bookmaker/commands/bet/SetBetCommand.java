package com.lawless.bookmaker.commands.bet;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.parsers.LongParser;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.dao.bet.BetDao;
import com.lawless.bookmaker.dao.bet.BetDaoImpl;
import com.lawless.bookmaker.dao.race.RaceDao;
import com.lawless.bookmaker.dao.race.RaceDaoImpl;
import com.lawless.bookmaker.dao.race_participant.RaceParticipantDao;
import com.lawless.bookmaker.dao.race_participant.RaceParticipantDaoImpl;
import com.lawless.bookmaker.domain.Bet;
import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.domain.User;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 08.12.16.
 */
public class SetBetCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(SetBetCommand.class);
    private RaceParticipantDao raceParticipantDao = RaceParticipantDaoImpl.getInstance();
    private BetDao betDao = BetDaoImpl.getInstance();
    private RaceDao raceDao = RaceDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        Map<String, Object> errors = new HashMap<>();
        Long raceParticipantId = Long.parseLong(model.findParameter("racePartId"));
        Long amount = model.findParameter("bet", new LongParser(errors));
        RaceParticipant raceParticipant = raceParticipantDao.read(raceParticipantId);
        if (!errors.isEmpty()) {
            model.setAttributes(errors);
            model.setAttribute("failedBetRacePartId", raceParticipantId);
            model.setAttribute("failedBet", model.findParameter("bet"));
            Long raceId = raceParticipant.getRace().getId();
            User user = (User) model.getSessionAttribute("user");
            Race race = raceDao.read(raceId, user);
            model.setAttribute("race", race);
            model.setAttribute("completed", isCompleted(race));
            LOGGER.info("Show detail for " + race);
            return "/views/race_detail.jsp";
        }
        User user = (User) model.getSessionAttribute("user");
        Bet bet = new Bet();
        bet.setAmount(amount);
        bet.setRaceParticipant(raceParticipant);
        bet.setUser(user);
        model.setAttributes(errors);
        betDao.create(bet);
        LOGGER.info("Set new bet:" + amount + " for " + raceParticipant.getParticipant().getRiderName());
        return "redirect:/app/race/detail?id=" + raceParticipant.getRace().getId();
    }

    private boolean isCompleted(Race race) {
        boolean result = false;
        for (RaceParticipant raceParticipant : race.getRaceParticipants()) {
            if (raceParticipant.isWin()) {
                result = true;
                break;
            }
        }
        return result;
    }
}
