package com.lawless.bookmaker.commands.race_participant;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.parsers.IntegerParser;
import com.lawless.bookmaker.common.Model;

import com.lawless.bookmaker.dao.race.RaceDao;
import com.lawless.bookmaker.dao.race.RaceDaoImpl;
import com.lawless.bookmaker.dao.race_participant.RaceParticipantDao;
import com.lawless.bookmaker.dao.race_participant.RaceParticipantDaoImpl;
import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.domain.User;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 06.12.16.
 */
public class SetCoefficientCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(SetCoefficientCommand.class);
    private RaceParticipantDao raceParticipantDao = RaceParticipantDaoImpl.getInstance();
    private RaceDao raceDao = RaceDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        Map<String, Object> errors = new HashMap<>();
        Long raceParticipantId = Long.parseLong(model.findParameter("racePartId"));
        Integer coefficient = model.findParameter("coefficient", new IntegerParser(errors));
        RaceParticipant raceParticipant = raceParticipantDao.read(raceParticipantId);
        if (!errors.isEmpty()) {
            model.setAttributes(errors);
            model.setAttribute("failedCoefficientRacePartId", raceParticipantId);
            model.setAttribute("failedCoefficient", model.findParameter("coefficient"));
            Long raceId = raceParticipant.getRace().getId();
            User user = (User) model.getSessionAttribute("user");
            Race race = raceDao.read(raceId, user);
            model.setAttribute("race", race);
            model.setAttribute("completed", isCompleted(race));
            LOGGER.info("Show detail for " + race);
            return "/views/race_detail.jsp";
        }
        raceParticipant.setCoefficient(coefficient);
        raceParticipantDao.update(raceParticipant);
        LOGGER.info("Set new coefficient:" + coefficient + " for " + raceParticipant.getParticipant().getRiderName());
        return "redirect:/app/race/detail?id=" + raceParticipant.getRace().getId();
    }

    private boolean isCompleted(Race race) {
        boolean result = false;
        for (RaceParticipant raceParticipant : race.getRaceParticipants()) {
            if (raceParticipant.isWin()) {
                result = true;
                break;
            }
        }
        return result;
    }
}
