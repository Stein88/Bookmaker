package com.lawless.bookmaker.commands.race_participant;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.dao.race_participant.RaceParticipantDao;
import com.lawless.bookmaker.dao.race_participant.RaceParticipantDaoImpl;
import com.lawless.bookmaker.domain.RaceParticipant;
import org.apache.log4j.Logger;

/**
 * Created by Stein on 19.12.16.
 */
public class SetWinnerCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(SetWinnerCommand.class);
    private RaceParticipantDao raceParticipantDao = RaceParticipantDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        Long raceParticipantId = Long.parseLong(model.findParameter("racePartId"));
        RaceParticipant raceParticipant = raceParticipantDao.read(raceParticipantId);
        raceParticipant.setWin(true);
        raceParticipantDao.update(raceParticipant);
        LOGGER.info("Set new winner for " + raceParticipant.getParticipant().getRiderName());
        return "redirect:/app/race/detail?id=" + raceParticipant.getRace().getId();
    }
}
