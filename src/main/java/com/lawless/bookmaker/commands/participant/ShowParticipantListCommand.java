package com.lawless.bookmaker.commands.participant;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.dao.participant.ParticipantDao;
import com.lawless.bookmaker.dao.participant.ParticipantDaoImpl;

/**
 * Created by Stein on 28.11.16.
 */
public class ShowParticipantListCommand implements Command {
    private ParticipantDao participantDao = ParticipantDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        model.setAttribute("participants", participantDao.readAll());
        return "/views/participant_list.jsp";
    }
}
