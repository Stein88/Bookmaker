package com.lawless.bookmaker.commands.participant;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.common.parsers.RiderHorseParser;
import com.lawless.bookmaker.common.parsers.StringRequiredParser;
import com.lawless.bookmaker.dao.participant.ParticipantDao;
import com.lawless.bookmaker.dao.participant.ParticipantDaoImpl;
import com.lawless.bookmaker.domain.Participant;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 28.11.16.
 */
public class CreateParticipantCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(CreateParticipantCommand.class);
    private ParticipantDao participantDao = ParticipantDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        Map<String, Object> errors = new HashMap<>();
        String riderName = model.findParameter("riderName", new RiderHorseParser(errors, 10));
        String horseName = model.findParameter("horseName", new RiderHorseParser(errors, 10));
        Participant participant = participantDao.findByNameAndHorse(riderName, horseName);
        if (participant != null) {
            LOGGER.info("This " + participant + " already created");
            errors.put("error", "This participant already created");
            model.setAttribute("riderName", riderName);
            model.setAttribute("horseName", horseName);
            model.setAttributes(errors);
            return "/views/participant_create.jsp";
        }
        Participant newParticipant = new Participant(riderName, horseName);
        participantDao.create(newParticipant);
        LOGGER.info("Create " + newParticipant);
        return "redirect:/app/participant/list";
    }

    void setParticipantDao(ParticipantDao participantDao) {
        this.participantDao = participantDao;
    }
}
