package com.lawless.bookmaker.commands.race;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.dao.race.RaceDao;
import com.lawless.bookmaker.dao.race.RaceDaoImpl;

/**
 * Created by Stein on 24.11.16.
 */

public class ShowRaceListCommand implements Command {
    RaceDao raceDao = RaceDaoImpl.getInstance();

    public String execute(Model model) {
        model.setAttribute("races", raceDao.readAll());
        return "/views/race_list.jsp";
    }
}
