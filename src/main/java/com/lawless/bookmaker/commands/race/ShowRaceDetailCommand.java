package com.lawless.bookmaker.commands.race;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.dao.race.RaceDao;
import com.lawless.bookmaker.dao.race.RaceDaoImpl;
import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.RaceParticipant;
import com.lawless.bookmaker.domain.User;
import org.apache.log4j.Logger;

/**
 * Created by Stein on 27.11.16.
 */
public class ShowRaceDetailCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(ShowRaceDetailCommand.class);
    private RaceDao raceDao = RaceDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        Long raceId = Long.parseLong(model.findParameter("id"));
        User user = (User) model.getSessionAttribute("user");
        Race race = raceDao.read(raceId, user);
        model.setAttribute("race", race);
        model.setAttribute("completed", isCompleted(race));
        LOGGER.info("Show detail for " + race);
        return "/views/race_detail.jsp";
    }

    private boolean isCompleted(Race race) {
        boolean result = false;
        for (RaceParticipant raceParticipant : race.getRaceParticipants()) {
            if (raceParticipant.isWin()) {
                result = true;
                break;
            }
        }
        return result;
    }
}
