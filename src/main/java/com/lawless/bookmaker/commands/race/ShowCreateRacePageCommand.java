package com.lawless.bookmaker.commands.race;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;
import com.lawless.bookmaker.dao.participant.ParticipantDao;
import com.lawless.bookmaker.dao.participant.ParticipantDaoImpl;

/**
 * Created by Stein on 27.11.16.
 */
public class ShowCreateRacePageCommand implements Command {
    private ParticipantDao participantDao = ParticipantDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        model.setAttribute("participants", participantDao.readAll());
        return "/views/race_create.jsp";
    }
}
