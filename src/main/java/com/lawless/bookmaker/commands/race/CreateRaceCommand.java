package com.lawless.bookmaker.commands.race;

import com.lawless.bookmaker.common.*;
import com.lawless.bookmaker.common.parsers.DateParser;
import com.lawless.bookmaker.common.parsers.Parser;
import com.lawless.bookmaker.dao.participant.ParticipantDao;
import com.lawless.bookmaker.dao.race.RaceDao;
import com.lawless.bookmaker.dao.participant.ParticipantDaoImpl;
import com.lawless.bookmaker.dao.race.RaceDaoImpl;
import com.lawless.bookmaker.dao.race_participant.RaceParticipantDao;
import com.lawless.bookmaker.domain.Participant;
import com.lawless.bookmaker.domain.Race;
import com.lawless.bookmaker.domain.RaceParticipant;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by Stein on 27.11.16.
 */
public class CreateRaceCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(CreateRaceCommand.class);
    private RaceDao raceDao = RaceDaoImpl.getInstance();
    private ParticipantDao participantDao = ParticipantDaoImpl.getInstance();

    @Override
    public String execute(Model model) {
        Map<String, Object> errors = new HashMap<>();
        Race race = new Race();
        race.setDate(model.findParameter("raceDate", new DateParser(errors)));
        race.setRaceParticipants(model.findParameter("participantIds", new RaceParticipantsParser(errors)));
        if (!errors.isEmpty()) {
            model.setAttribute("raceDate", model.findParameter("raceDate"));
            model.setAttribute("participants", participantDao.readAll());
            model.setAttributes(errors);
            return "/views/race_create.jsp";
        }
        raceDao.create(race);
        LOGGER.info("Created new" + race);
        return "redirect:/app/race/list";
    }

    private class RaceParticipantsParser implements Parser<Collection<RaceParticipant>> {
        private Map<String, Object> errors;

        RaceParticipantsParser(Map<String, Object> errors) {
            this.errors = errors;
        }

        @Override
        public Collection<RaceParticipant> parse(String key, String[] params) {
            Collection<RaceParticipant> raceParticipants = new ArrayList<>();
            List<Long> ids = new ArrayList<>();
            if (params == null || params.length == 0) {
                errors.put(key + "_error", "Please select at least one participant");
                return raceParticipants;
            }
            for (String param : params) {
                ids.add(new Long(param));
            }
            Collection<Participant> participants = participantDao.findByIds(ids);
            for (Participant participant : participants) {
                RaceParticipant raceParticipant = new RaceParticipant();
                raceParticipant.setParticipant(participant);
                raceParticipants.add(raceParticipant);
            }
            return raceParticipants;
        }
    }

    void setParticipantDao(ParticipantDao participantDao) {
        this.participantDao = participantDao;
    }
}
