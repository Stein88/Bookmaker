package com.lawless.bookmaker.commands;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;

/**
 * Created by Stein on 28.11.16.
 */
public class ShowLoginPageCommand implements Command{
    @Override
    public String execute(Model model) {
        return "/views/login.jsp";
    }
}
