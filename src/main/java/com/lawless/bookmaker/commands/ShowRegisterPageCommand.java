package com.lawless.bookmaker.commands;

import com.lawless.bookmaker.common.Command;
import com.lawless.bookmaker.common.Model;

/**
 * Created by Stein on 28.11.16.
 */
public class ShowRegisterPageCommand implements Command {
    @Override
    public String execute(Model model) {
        return "/views/registration.jsp";
    }
}
