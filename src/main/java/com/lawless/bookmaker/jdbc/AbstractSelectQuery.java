package com.lawless.bookmaker.jdbc;

import com.lawless.bookmaker.domain.Participant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Stein on 05.12.16.
 */
public abstract class AbstractSelectQuery {

    protected Collection<Participant> obtainParticipants(ResultSet resultSet) throws SQLException {
        Collection<Participant> participantsList = new ArrayList<>();
        while (resultSet.next()) {
            Participant participant = new Participant();
            participant.setId(resultSet.getLong("participant_id"));
            participant.setRiderName(resultSet.getString("rider_name"));
            participant.setHorseName(resultSet.getString("horse_name"));
            participantsList.add(participant);
        }
        return participantsList;
    }
}
