package com.lawless.bookmaker.jdbc;

import java.sql.SQLException;

/**
 * Created by Stein on 04.12.16.
 */
public interface InsertQueryCommand<T> extends QueryCommand {
    T extractInserted(Long id) throws SQLException;
}
