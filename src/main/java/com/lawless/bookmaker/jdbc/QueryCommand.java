package com.lawless.bookmaker.jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Stein on 04.12.16.
 */
public interface QueryCommand {
    String getQuery();
    void setupStatement(PreparedStatement preparedStatement) throws SQLException;
}
