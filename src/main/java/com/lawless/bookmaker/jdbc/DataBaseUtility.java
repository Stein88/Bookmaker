package com.lawless.bookmaker.jdbc;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;

import java.sql.*;


public class DataBaseUtility {
    private static final Logger LOGGER = Logger.getLogger(DataBaseUtility.class);
    private static BasicDataSource dataSource;

    private DataBaseUtility() {
    }

    private static BasicDataSource getDataSource() {
        if (dataSource == null) {
            BasicDataSource ds = new BasicDataSource();
            ds.setDriverClassName("com.mysql.jdbc.Driver");
            ds.setUrl("jdbc:mysql://localhost:3306/bookmaker?autoReconnect=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8");
            ds.setUsername("root");
            ds.setPassword("q0w9e8p1o2i3");
            dataSource = ds;
        }
        return dataSource;
    }

    private static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }


    public static <T> T execute(InsertQueryCommand<T> command) {
        String sql = command.getQuery();
        try (Connection connection = DataBaseUtility.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ) {
            command.setupStatement(preparedStatement);
            int status = preparedStatement.executeUpdate();
            if (status != 1) {
                throw new SQLException("Creating failed, no rows affected.");
            }
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    Long generatedId = generatedKeys.getLong(1);
                    return command.extractInserted(generatedId);
                } else {
                    throw new SQLException("Creating failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public static <T> T execute(SelectQueryCommand<T> command) {
        String sql = command.getQuery();
        try (Connection connection = DataBaseUtility.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            command.setupStatement(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            return command.parseResultSet(resultSet);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public static <T> T execute(UpdateQueryCommand<T> command) {
        String sql = command.getQuery();
        try (Connection connection = DataBaseUtility.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            command.setupStatement(preparedStatement);
            preparedStatement.executeUpdate();
            return command.returnUpdatedObject();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

}
