package com.lawless.bookmaker.jdbc;

import java.sql.SQLException;

/**
 * Created by Stein on 07.12.16.
 */
public interface UpdateQueryCommand<T> extends QueryCommand {
    T returnUpdatedObject() throws SQLException;
}
