package com.lawless.bookmaker.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Stein on 04.12.16.
 */
public interface SelectQueryCommand<T> extends QueryCommand {
    T parseResultSet(ResultSet resultSet) throws SQLException;
}
