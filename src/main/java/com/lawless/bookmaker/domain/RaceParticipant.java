package com.lawless.bookmaker.domain;

import java.util.Collection;

/**
 * Created by Stein on 29.11.16.
 */
public class RaceParticipant extends BaseEntity {
    private Integer coefficient;
    private boolean win;
    private Race race;
    private Participant participant;
    private Collection<Bet> bets;

    public RaceParticipant() {
    }

    public RaceParticipant(Race race, Participant participant) {
        this.race = race;
        this.participant = participant;
    }

    public RaceParticipant(Long id, Integer coefficient, Participant participant, Race race, Collection<Bet> bets) {
        this.setId(id);
        this.coefficient = coefficient;
        this.participant = participant;
        this.race = race;
        this.bets = bets;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public Collection<Bet> getBets() {
        return bets;
    }

    public void setBets(Collection<Bet> bets) {
        this.bets = bets;
    }

    public Integer getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Integer coefficient) {
        this.coefficient = coefficient;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RaceParticipant{");
        sb.append("coefficient=").append(coefficient);
        sb.append(", participant=").append(participant);
        sb.append(", bets=").append(bets);
        sb.append('}');
        return sb.toString();
    }
}
