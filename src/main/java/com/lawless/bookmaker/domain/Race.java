package com.lawless.bookmaker.domain;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Stein on 24.11.16.
 */
public class Race extends BaseEntity {
    private Date date;
    private Collection<RaceParticipant> raceParticipants;

    public Race() {
    }

    public Race(Long id, Date date) {
        this.setId(id);
        this.date = date;
    }

    public Collection<RaceParticipant> getRaceParticipants() {
        return raceParticipants;
    }

    public void setRaceParticipants(Collection<RaceParticipant> raceParticipants) {
        this.raceParticipants = raceParticipants;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Race{");
        sb.append("date=").append(date);
        sb.append('}');
        return sb.toString();
    }
}
