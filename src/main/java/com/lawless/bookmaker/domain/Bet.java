package com.lawless.bookmaker.domain;

/**
 * Created by Stein on 24.11.16.
 */
public class Bet extends BaseEntity{
    private Long amount;
    private RaceParticipant raceParticipant;
    private User user;

    public Bet() {
    }

    public Bet(Long amount, RaceParticipant raceParticipant, User user) {
        this.amount = amount;
        this.raceParticipant = raceParticipant;
        this.user = user;
    }

    public Bet(Long id, Long amount, RaceParticipant raceParticipant, User user) {
        this.setId(id);
        this.amount = amount;
        this.raceParticipant = raceParticipant;
        this.user = user;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public RaceParticipant getRaceParticipant() {
        return raceParticipant;
    }

    public void setRaceParticipant(RaceParticipant raceParticipant) {
        this.raceParticipant = raceParticipant;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Bet{");
        sb.append("amount=").append(amount);
        sb.append(", raceParticipant=").append(raceParticipant);
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }
}
