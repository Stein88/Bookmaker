package com.lawless.bookmaker.domain;

/**
 * Created by Stein on 24.11.16.
 */
public enum UserRole {
    ADMIN, CLIENT, BOOKMAKER;

    public static UserRole fromString(String string) {
        UserRole result = null;
        for (UserRole role : values()) {
            if (role.toString().equalsIgnoreCase(string)) {
                result = role;
                break;
            }
        }
        return result;
    }
}
