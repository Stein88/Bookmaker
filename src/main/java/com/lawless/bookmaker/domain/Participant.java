package com.lawless.bookmaker.domain;

/**
 * Created by Stein on 24.11.16.
 */
public class Participant extends BaseEntity {
    private String riderName;
    private String horseName;

    public Participant() {
    }

    public Participant(Long id, String riderName, String horseName) {
        this.setId(id);
        this.riderName = riderName;
        this.horseName = horseName;
    }

    public Participant(String riderName, String horseName) {
        this.riderName = riderName;
        this.horseName = horseName;
    }

    public String getRiderName() {
        return riderName;
    }

    public void setRiderName(String riderName) {
        this.riderName = riderName;
    }

    public String getHorseName() {
        return horseName;
    }

    public void setHorseName(String horseName) {
        this.horseName = horseName;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Participant{");
        sb.append("riderName='").append(riderName).append('\'');
        sb.append(", horseName='").append(horseName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
