package com.lawless.bookmaker.domain;

/**
 * Created by Stein on 04.12.16.
 */
public abstract class BaseEntity {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
