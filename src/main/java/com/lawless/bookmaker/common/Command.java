package com.lawless.bookmaker.common;

/**
 * Created by Stein on 24.11.16.
 */
public interface Command {
    String execute(Model model);
}
