package com.lawless.bookmaker.common.parsers;

import java.util.Map;

/**
 * Created by Stein on 20.12.16.
 */
public class StringRequiredParser extends StringParser implements Parser<String> {
    private Map<String, Object> errors;
    private String errorMsg;

    public StringRequiredParser(Map<String, Object> errors, String errorMsg) {
        this.errors = errors;
        this.errorMsg = errorMsg;

    }

    @Override
    public String parse(String key, String[] params) {
        String str = super.parse(key, params);
        if (str == null || str.length() == 0) {
            errors.put(key + "_error", errorMsg);
        }
        return str;
    }
}
