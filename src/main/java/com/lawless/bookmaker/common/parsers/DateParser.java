package com.lawless.bookmaker.common.parsers;

import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by Stein on 28.11.16.
 */
public class DateParser implements Parser<Date> {
    private static final Logger LOGGER = Logger.getLogger(DateParser.class);
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private Map<String, Object> errors;

    public DateParser(Map<String, Object> errors) {
        this.errors = errors;
    }

    @Override
    public Date parse(String key, String[] params) {
        Date result = null;
        try {
            if (params != null && params.length != 0) {
                result =  DATE_FORMAT.parse(params[0]);
            }
        } catch (ParseException e) {
            LOGGER.warn(e.getMessage(), e);
            errors.put(key + "_error", "Date format is incorrect");
        }
        return result;
    }
}
