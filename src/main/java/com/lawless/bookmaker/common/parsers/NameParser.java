package com.lawless.bookmaker.common.parsers;


import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Stein on 19.12.16.
 */
public class NameParser extends StringRequiredParser implements Parser<String> {
    private static final Pattern VALID_NAME_REGEX =
            Pattern.compile("^[\\p{L} .'-]+$", Pattern.CASE_INSENSITIVE);
    private Map<String, Object> errors;

    public NameParser(Map<String, Object> errors) {
        super(errors, "Name is required");
        this.errors = errors;
    }

    @Override
    public String parse(String key, String[] params) {
        String name = super.parse(key, params);
        if (errors.containsKey(key + "_error")) {
            return name;
        }
        if (!validate(name)) {
            errors.put(key + "_error", "Your name don't match pattern, should be like Tim Cook");
            errors.put("name_error", "Your name don't match pattern, should be like Tim Cook");
        }
        return name;
    }

    private static boolean validate(String name) {
        Matcher matcher = VALID_NAME_REGEX.matcher(name);
        return matcher.find();
    }
}
