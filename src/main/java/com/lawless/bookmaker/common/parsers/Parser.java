package com.lawless.bookmaker.common.parsers;

/**
 * Created by Stein on 28.11.16.
 */
public interface Parser<T> {
    T parse(String key, String[] params);
}
