package com.lawless.bookmaker.common.parsers;


import java.util.Map;

/**
 * Created by Stein on 19.12.16.
 */
public class PasswordParser extends StringRequiredParser implements Parser<String> {
    private Map<String, Object> errors;

    public PasswordParser(Map<String, Object> errors) {
        super(errors, "Password is required");
        this.errors = errors;
    }

    @Override
    public String parse(String key, String[] params) {
        String password = super.parse(key,params);
        if (errors.containsKey(key + "_error")) {
            return password;
        }
        if (password.length() < 6) {
            errors.put(key + "_error", "Should contains more than 6 characters");
        }
        return password;
    }
}
