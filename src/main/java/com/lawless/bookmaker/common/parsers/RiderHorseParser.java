package com.lawless.bookmaker.common.parsers;

import java.util.Map;

/**
 * Created by Stein on 21.12.16.
 */
public class RiderHorseParser extends StringRequiredParser implements Parser<String> {
    private Map<String, Object> errors;
    private int maxLength;

    public RiderHorseParser(Map<String, Object> errors, int maxLength) {
        super(errors, "Name is required");
        this.errors = errors;
        this.maxLength = maxLength;
    }

    @Override
    public String parse(String key, String[] params) {
        String str = super.parse(key, params);
        if (errors.containsKey(key + "_error")){
            return str;
        }
        if (str.length() > maxLength){
            errors.put(key+ "_error", "Should contains no more than 10 characters");
        }
        return super.parse(key, params);
    }
}
