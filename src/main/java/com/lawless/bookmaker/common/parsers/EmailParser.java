package com.lawless.bookmaker.common.parsers;

import com.lawless.bookmaker.common.parsers.Parser;
import com.lawless.bookmaker.common.parsers.StringRequiredParser;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Stein on 19.12.16.
 */
public class EmailParser extends StringRequiredParser implements Parser<String> {
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private Map<String, Object> errors;

    public EmailParser(Map<String, Object> errors) {
        super(errors, "Email is required");
        this.errors = errors;
    }

    @Override
    public String parse(String key, String[] params) {
        String email = super.parse(key, params);
        if (errors.containsKey(key + "_error")) {
            return email;
        }
        if (!validate(email)) {
            errors.put(key + "_error", "Your email don't match pattern, should be like example@mail.com");
        }
        return email;
    }

    private static boolean validate(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }
}
