package com.lawless.bookmaker.common.parsers;

import org.apache.log4j.Logger;

import java.util.Map;

/**
 * Created by Stein on 20.12.16.
 */
public abstract class NumberParser<T extends Number> implements Parser<T> {
    private static final Logger LOGGER = Logger.getLogger(LongParser.class);
    private Map<String, Object> errors;

    NumberParser(Map<String, Object> errors) {
        this.errors = errors;
    }

    Long parseLong(String key, String[] params) {
        Long result = null;
        if (params != null && params.length != 0) {
            String param = params[0];
            if (param != null && !param.isEmpty()) {
                try {
                    result = Long.parseLong(param);
                } catch (NumberFormatException e) {
                    LOGGER.warn(e.getMessage(), e);
                    errors.put(key + "_error", "Should be number");
                }
            } else {
                errors.put(key+ "_error", "Shouldn't be null");
            }
        }
        return result;
    }
}
