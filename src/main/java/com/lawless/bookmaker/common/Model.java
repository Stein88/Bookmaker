package com.lawless.bookmaker.common;


import com.lawless.bookmaker.common.parsers.Parser;

import java.util.Map;

/**
 * Created by Stein on 24.11.16.
 */
public interface Model {
    void setAttribute(String key, Object o);

    void setAttributes(Map<String, Object> attributes);

    String findParameter(String key);

    <T> T findParameter(String key, Parser<T> parser);

    void putSessionAttribute(String s, Object o);

    Object getSessionAttribute(String s);
}
