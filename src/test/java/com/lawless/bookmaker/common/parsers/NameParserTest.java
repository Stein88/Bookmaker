package com.lawless.bookmaker.common.parsers;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by Stein on 21.12.16.
 */
public class NameParserTest {
    @Test
    public void testParseSuccess() throws Exception {
        String[] params = new String[] {"hgfghjghj"};
        Map<String, Object> errors = new HashMap<>();
        Parser<String> parser = new NameParser(errors);
        String result = parser.parse("test", params);

        Assert.assertTrue(errors.isEmpty());
        Assert.assertEquals(params[0], result);
    }

    @Test
    public void testParseSpecSymbols() throws Exception {
        String[] params = new String[] {"hg!"};
        Map<String, Object> errors = new HashMap<>();
        Parser<String> parser = new NameParser(errors);
        String result = parser.parse("test", params);

        Assert.assertEquals(1, errors.size());
        Assert.assertTrue(errors.containsKey("test_error"));
        Assert.assertEquals(params[0], result);
    }

    @Test
    public void testParseNull() throws Exception {
        String[] params = new String[] {null};
        Map<String, Object> errors = new HashMap<>();
        Parser<String> parser = new NameParser(errors);
        String result = parser.parse("test", params);

        Assert.assertEquals(1, errors.size());
        Assert.assertTrue(errors.containsKey("test_error"));
        Assert.assertEquals(params[0], result);
    }

}